from django.test import TestCase

# Create your tests here.
class WebsiteTestCase(TestCase):
    fixtures = ['initial.json', ]

    def test_index_page(self):
        response = self.client.get(reverse('licorne_review:index'))
        self.assertContains(response, "Derniers articles")
        self.assertEqual(type(response.context['latest_articles']), QuerySet)
        self.assertEqual(len(response.context['latest_articles']), 3)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('index.html')

    def test_article_page(self):
        response = self.client.get(reverse('licorne_review:article'))
        self.assertContains(response, "")
        self.assertEqual(type(response.context['article']), QuerySet)
        self.assertEqual(len(response.context['article']), 1)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('article.html')
