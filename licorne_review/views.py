from django.shortcuts import render,get_object_or_404
from licorne_review.models import Article, Commentaire
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from licorne_review.filters import ArticleFilter
from licorne_review.form import CommentaireForm
# Create your views here.


def index(request):
    list_articles = Article.objects.order_by("release_date").all()
    paginator = Paginator(list_articles, 3) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        latest_articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        latest_articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        latest_articles = paginator.page(paginator.num_pages)

    return render(request, 'index.html', { 'latest_articles' : latest_articles })



def article(request, slug):
    article = get_object_or_404(Article, slug=slug)

    commentaires = Commentaire.objects.filter(article_id=article.id)

    commentaire = Commentaire(article_id=article.id)
    form = CommentaireForm(request.POST, instance=commentaire)
    if form.is_valid():
        form.save()
    form = CommentaireForm()

    return render( request, 'article.html', { 'article' : article, 'commentaires' : commentaires, 'form' : form} )



def search(request):
    list_articles = Article.objects.all()

    filter = ArticleFilter(request.GET, queryset = list_articles)

    return render(request, 'search.html', { 'articles' : filter })
