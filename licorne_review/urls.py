from django.urls import path
from licorne_review import views
app_name = 'licorne_review'

urlpatterns = [
    path('', views.index, name='index'),
    path('article/<slug:slug>', views.article, name='article'),
    path('recherche',views.search, name="search")
]
