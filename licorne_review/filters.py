from licorne_review.models import Article
import django_filters

class ArticleFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr="icontains", label="Titre")
    class Meta:
        model = Article
        fields = ['title', ]
