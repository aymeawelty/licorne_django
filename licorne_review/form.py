from django import forms
from .models import Commentaire

class CommentaireForm( forms.ModelForm ):
    class Meta:
        model = Commentaire
        labels = {
            'author' : ('Nom'),
            'content' : ("Votre commentaire"),
        }
        exclude = ('article',)
