from django.apps import AppConfig


class LicorneReviewConfig(AppConfig):
    name = 'licorne_review'
